# -*- coding: utf-8 -*-
import os

DJANGO_ROOT = os.path.dirname(os.path.abspath(__file__)) + '/'

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Olevinsky VS', 'olevinsky.v.s@gmail.com'),
)

MANAGERS = ADMINS

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
#         'NAME': '',                      # Or path to database file if using sqlite3.
#         'USER': '',                      # Not used with sqlite3.
#         'PASSWORD': '',                  # Not used with sqlite3.
#         'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
#         'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
#     }
# }

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': DJANGO_ROOT + 'cache/',
        'TIMEOUT': 100000,
        'OPTIONS': {
            'MAX_ENTRIES': 1000
        }
    }
}

TIME_ZONE = 'Europe/Moscow'

LANGUAGE_CODE = 'ru-RU'

SITE_ID = 1

USE_I18N = False
USE_L10N = False

MEDIA_ROOT = DJANGO_ROOT + 'media/'

MEDIA_URL = '/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = '%+g*@2n=8n$mh1qm)p8^qla2tgzc$=#sz01ytgp(1m1&)jp&5%'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'fcolors.urls'

TEMPLATE_DIRS = (
    DJANGO_ROOT + 'templates',
)

INSTALLED_APPS = (
    'fcolors.twitterapp',
)