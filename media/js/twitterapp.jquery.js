(function($){
    var $name = 'twitterapp',
    methods = {
        init: function(){
            return this.each(function(){
                var $this = $(this),
                data = {
                    username: false,
                    $loader: $('.i-ajax_loader', $this),
                    $input: $('.i-username_input', $this),
                    $tweets: $('.b-tweets', $this),
                    $errors: $('.b-errors', $this)
                };

                $this.data($name, data);
                $this[$name]('events');
            });
        },
        events: function(){
            var $this = $(this),
            data = $this.data($name);

            data.$input.unbind('keyup');
            data.$input.keyup(function(e){
                if (e.keyCode == 13) {
                    data.$input.unbind('keyup');
                    data.username = data.$input.val();
                    $this[$name]('submit');
                }
            });
        },
        submit: function(){
            var $this = $(this),
            data = $this.data($name);

            data.$loader.show();
            data.$tweets.html('');
            data.$input.attr('disabled', 'disabled');
            $this[$name]('clear_errors');

            $.ajax({
                url: '/',
                type: 'POST',
                data: {'user_id': data.username},
                dataType: 'json',
                success: function(success_data, status) {
                    $this[$name]('submit_complete');
                    if (success_data.errno > 0) {
                        $this[$name]('show_errors', success_data.errors);
                    } else {
                        $this[$name]('success', success_data.replies_html);
                    }
                }
            });
        },
        submit_complete: function() {
            var $this = $(this),
            data = $this.data($name);

            data.$loader.hide();
            data.$input.removeAttr('disabled');
            $this[$name]('events');
        },
        success: function(tweets_html) {
            var $this = $(this),
            data = $this.data($name);

            data.$tweets.html(tweets_html);
        },
        clear_errors: function() {
            var $this = $(this),
            data = $this.data($name);

            $('*', data.$errors).remove();
        },
        show_errors: function(errors) {
            var $this = $(this),
            $P = $('<p class="__error"></p>'),
            data = $this.data($name);

            $.each(errors, function(k, error) {
                $P.text(error);
                data.$errors.append($P);
            });
        }
    };
    $.fn[$name] = $.namespace(methods);
})(jQuery);