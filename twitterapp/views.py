# -*- coding: utf-8 -*-
import twitter
import json

from django.core.cache import cache
from django.http import HttpResponse
from django.template import (
    loader,
    Context,
)
from django.shortcuts import render_to_response


def replies(request):
    u"""
        Показывает последние 10 реплаев твиттер аккаунта.
    """

    max_replies = 10
    max_tweets = 50
    max_deep = 5
    deep = 0
    replies = []
    replies_dict = {} # Будем использовать для групировки
    max_id = None
    replies_html = ''
    errors = []

    if request.method == "POST":
        api = twitter.Api()
        user_id = request.POST.get('user_id') or 'futurecolors'

        # Пытаемся заполнить реплаи пока не наберем необходимое количество
        while len(replies) <= max_replies and deep <= max_deep:
            deep += 1
            try:
                timeline = api.GetUserTimeline(id=user_id, count=max_tweets, include_entities=True, max_id=max_id)
            except twitter.TwitterError, e:
                if e.message == 'Not found':
                    errors.append(u'Пользователя %s не существует.' % user_id)
                break
            except:
                errors.append(u'Не удалось получить данные пользователя.')
                break

            if not timeline:
                break

            for tweet in timeline:
                _reply_id = tweet.GetInReplyToStatusId()
                if _reply_id:
                    reply_to = cache.get(_reply_id)
                    if not reply_to:
                        try:
                            reply_to = api.GetStatus(_reply_id).AsDict()
                            cache.set(_reply_id, reply_to)
                        except twitter.TwitterError:
                            continue

                    if _reply_id in replies_dict:
                        tweet_dict = tweet.AsDict()
                        if tweet_dict not in replies_dict[_reply_id]['tweets']:
                            replies_dict[_reply_id]['tweets'].append(tweet_dict)
                    else:
                        reply = {'tweets': [tweet.AsDict(), ], 'reply_to': reply_to}
                        replies_dict[_reply_id] = reply
                        replies.append(reply)

                    if len(replies) >= max_replies:
                        break

                max_id = tweet.id

        if not errors:
            if replies:
                t = loader.get_template('_tweets.html')
                c = Context({'replies': replies[:max_replies]})
                replies_html = t.render(c)
            else:
                errors.append(u'В последних %d твитах, нет реплаев' % (max_deep * max_tweets))

        return HttpResponse(json.dumps({
            'replies_html': replies_html,
            'errors': errors,
            'errno': len(errors),
        }), mimetype="application/json")

    return render_to_response('index.html', {
        'replies': replies
    })
